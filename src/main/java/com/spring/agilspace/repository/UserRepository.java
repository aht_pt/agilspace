package com.spring.agilspace.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.spring.agilspace.model.User;


@Repository
public interface UserRepository extends JpaRepository<User, Integer>{

	User findByUsername(String username);
}
