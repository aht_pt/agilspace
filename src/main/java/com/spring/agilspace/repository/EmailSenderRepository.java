package com.spring.agilspace.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.spring.agilspace.model.EmailSender;

@Repository
public interface EmailSenderRepository extends JpaRepository<EmailSender, Integer>{
	
}
