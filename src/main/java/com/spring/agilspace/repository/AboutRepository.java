package com.spring.agilspace.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.spring.agilspace.model.About;

@Repository
public interface AboutRepository extends JpaRepository<About, Integer>{
	
}
