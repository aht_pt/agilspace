package com.spring.agilspace.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.spring.agilspace.model.Gallery;

@Repository
public interface GalleryRepository extends JpaRepository<Gallery, Integer>{

}
