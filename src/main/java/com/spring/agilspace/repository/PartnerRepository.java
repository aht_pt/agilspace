package com.spring.agilspace.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.spring.agilspace.model.Partner;

@Repository
public interface PartnerRepository extends JpaRepository<Partner,Integer>{

	Partner findByName(String name);
}
