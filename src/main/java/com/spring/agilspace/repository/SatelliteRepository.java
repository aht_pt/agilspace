package com.spring.agilspace.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.spring.agilspace.model.Satellite;

@Repository
public interface SatelliteRepository extends JpaRepository<Satellite, Integer>{
	List<Satellite> findByPartnerid(int partnerid);
	
	@Transactional
	@Query("SELECT s FROM Satellite s WHERE s.partnerid = ? AND s.name = ?")
	Satellite searchSatellite(int partnerid, String name);
}
