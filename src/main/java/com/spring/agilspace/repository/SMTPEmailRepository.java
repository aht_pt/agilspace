package com.spring.agilspace.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.spring.agilspace.model.SMTPEmail;

@Repository
public interface SMTPEmailRepository extends JpaRepository<SMTPEmail, Integer>{

}
