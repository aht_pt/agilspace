package com.spring.agilspace.util;

public class Constants {

	
	public static final int ROLE_ADMIN = 1;
	public static final String ADMIN = "ADMIN";
	public static final String DEFAULT_AUTHEN = "admin";
	//contact page
	public static final String DEFAULT_EMAIL = "sale@agilspace.com";
	public static final String DEFAULT_PHONE = "(65) 6521 7888";
	public static final String DEFAULT_ADDRESS = "1 Ang Mo Kio Electronics Park Road #06-02 Singapore 567710";
	public static final String CONTACT_US = "Contact us";
	public static final String ELECTRONICS = "ST Electronics";
	public static final String SEND = "send";
	public static final String NAME = "name";
	public static final String EMAIL_ADDRESS = "Email address";
	public static final String CONTACT_NUMBER = "Contact number";
	public static final String COMPANY_NAME = "Company name";
	public static final String SELECT_ENQUIRY = "Select enquiry";
	public static final String ENQUIRY = "Enquiry";
	public static final String ENQUIRY_OPTION = "enquiry1,enquiry2";
	public static final String MESSAGE = "Message";
	//display satellite of partner
	public static final int ONLY_IMAGE = 1;
	public static final int FULL_INFORMATION = 2;
	//folder name
	public static final String FOLDER_PARTNER = "Partner";
	public static final String FOLDER_SATELLITE = "Satellite";
	public static final String FOLDER_GALLERY = "gallery";
	public static final String FOLDER_BACKGROUND = "background";
	public static final String FOLDER_IMAGE = "images";
	public static final String BACKGROUND_FILE_1 = "bkg-home.png";
	public static final String BACKGROUND_FILE_2 = "star.png";
	
	//menu
	public static final String ABOUT = "ABOUT US";
	public static final String GEOPORTAL = "GEOPORTAL";
	public static final String GEOSERVICES = "GEOSERVICES";
	public static final String DATE_WAREHOUSE = "DATE WAREHOUSE";
	public static final String INSIGHTS = "INSIGHTS";
	public static final String GEOAPPS_SOFTWARE = "GEOAPPS & SOFTWARE";
	public static final String TRAINING_CONSULTANCY = "TRAINING & CONSULTANCY";
	public static final String SATELLITE = "SATELLITE";
	public static final String TELEOS_SERIES = "TELEOS SERIES";
	public static final String PARTNERS = "PARTNERS";
	public static final String INTERGRATED_SERVICES = "INTERGRATED SERVICES";
	public static final String PROGRAME_MANAGEMENT = "PROGRAMME MANAGEMENT";
	public static final String LAUNCH_MANAGEMENT = "LAUNCH MANAGEMENT";
	public static final String GCS = "GCS AND ANTENNA SERVICES";
	public static final String NEWS = "NEWS";
	public static final String CALENDAR = "CALENDAR OF EVENTS";
	public static final String ENEWSLETTER = "eNEWSLETTER";
	public static final String GALLERY = "GALLERY";
	public static final String CONTACT = "CONTACT US";
	
	public static final String HYPHEN = "-";
	public static final String COMMA = ",";
	
	public static final String SMTP_EMAIL = "agilspacesender@gmail.com";
	public static final String SMTP_PASSWORD = "agilspace@2019";
	
	//home page
	public static final String APP_NAME = "Agilspace";
	public static final String APP_INTRO = "Multi-Modality Multi-Satellite Constellation for High Revisit Geospatial Intelligence";
	public static final String EXPLORE = "Explore your space journey ";
	public static final String START = "Start";
	public static final String SINGAPORE_INTRO = "Singapore 's very own Earth Observation Satellites";
	
}
