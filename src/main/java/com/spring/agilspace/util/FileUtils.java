package com.spring.agilspace.util;

import java.io.File;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.web.multipart.MultipartFile;

public class FileUtils {
	
	private static final Logger logger = Logger.getLogger(FileUtils.class);

	public static void uploadImagePartner(HttpServletRequest request,MultipartFile logo, String name){
		String path = request.getServletContext().getRealPath("assets").concat(File.separator).concat(name);
		File fileFolder = new File(path);
    	if(!fileFolder.exists()){
    		fileFolder.mkdir();
    	}
    	String fileName = logo.getOriginalFilename();
    	path = path.concat(File.separator).concat(fileName);
    	File fileImageSave = new File(path);
    	try {
    		logo.transferTo(fileImageSave);
		} catch (Exception e) {
			logger.debug("error save file: " + e.getMessage());
		}
	}
	public static void uploadGallery(HttpServletRequest request,MultipartFile image){
		String path = request.getServletContext().getRealPath("assets").concat(File.separator).concat(Constants.FOLDER_GALLERY);
		File fileFolder = new File(path);
    	if(!fileFolder.exists()){
    		fileFolder.mkdir();
    	}
    	String fileName = image.getOriginalFilename();
    	path = path.concat(File.separator).concat(fileName);
    	File fileImageSave = new File(path);
    	try {
    		image.transferTo(fileImageSave);
		} catch (Exception e) {
			logger.debug("error save file: " + e.getMessage());
		}
	}
	
	public static void uploadBackground(HttpServletRequest request,MultipartFile image, String filename){
		String path = request.getServletContext().getRealPath("assets").concat(File.separator).concat(Constants.FOLDER_IMAGE);
		File fileFolder = new File(path);
    	if(!fileFolder.exists()){
    		fileFolder.mkdir();
    	}
    	path = path.concat(File.separator).concat(filename);
    	File fileImageSave = new File(path);
    	try {
    		image.transferTo(fileImageSave);
		} catch (Exception e) {
			logger.debug("error save file: " + e.getMessage());
		}
	}
	
	public static void deleteGallery(HttpServletRequest request, String path){
		String pathPartner = request.getServletContext().getRealPath("assets").concat(File.separator);
		File file = new File(pathPartner.concat(path));
		if(file.exists()){
			file.delete();
		}
	}
	
	public static void moveFolder(HttpServletRequest request, String oldFolder, String newFolder){
		String pathPartner = request.getServletContext().getRealPath("assets").concat(File.separator);
		oldFolder = pathPartner.concat(oldFolder);
		newFolder = pathPartner.concat(newFolder);
		File dirOld = new File(oldFolder);
		File dirNew = new File(newFolder);
		try {
			org.apache.commons.io.FileUtils.moveDirectory(dirOld, dirNew);
		} catch (Exception e) {
			logger.debug("error move folder: " +e.getMessage());
		}
	}
	
	public static void deleteImagePartner(HttpServletRequest request, String path){
		String pathPartner = request.getServletContext().getRealPath("assets").concat(File.separator);
		File file = new File(pathPartner.concat(path));
		if(file.exists()){
			file.delete();
		}
	}
	
	public static void deleteLogoPartner(HttpServletRequest request, String name){
		String path = request.getServletContext().getRealPath("assets").concat(File.separator).concat(name);
		File fileFolder = new File(path);
    	if(fileFolder.exists()){
    		try {
    			fileFolder.delete();
			} catch (Exception e) {
				logger.debug("error delete file: " + e.getMessage());
			}
    	}
	}
}
