package com.spring.agilspace.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.spring.agilspace.model.About;
import com.spring.agilspace.service.AboutService;

@Controller
public class AboutController {

    @Autowired
    AboutService aboutService;

    @RequestMapping(value ="/admin/about", method = RequestMethod.GET)
    public String aboutGet(@ModelAttribute("success") String success,Model model){
        List<About> lsAbout = aboutService.getAbout();
        if (lsAbout != null && !lsAbout.isEmpty()){
            About about = lsAbout.get(0);
            model.addAttribute("about", about);
        }
        if(!success.isEmpty()){
			model.addAttribute("success", success);
		}
        return "admin/about";
    }

	@RequestMapping(value = "/admin/about", method = RequestMethod.POST)
	public String aboutPost(
			@RequestParam(value = "title", required = false) String title,
			@RequestParam(value = "leftTitle", required = false) String leftTitle,
			@RequestParam(value = "leftContent", required = false) String leftContent,
			@RequestParam(value = "rightTitle", required = false) String rightTitle,
			@RequestParam(value = "rightContent", required = false) String rightContent,
			@RequestParam(value = "visitText", required = false) String visitText,
			Model model, RedirectAttributes ra) {
		List<About> lsAbout = aboutService.getAbout();
		if(lsAbout == null || lsAbout.isEmpty()){
			About about = new About();
			about.setTitle(title);
			about.setLeftTitle(leftTitle);
			about.setRightTitle(rightTitle);
			about.setLeftContent(leftContent);
			about.setRightContent(rightContent);
			about.setVisitText(visitText);
			aboutService.saveAbout(about);
		}else if (!lsAbout.isEmpty()) {
		
			About about = lsAbout.get(0);
			if (title != null) {
				about.setTitle(title);
			}
			if (leftTitle != null) {
				about.setLeftTitle(leftTitle);
			}
			if (leftContent != null) {
				about.setLeftContent(leftContent);
			}
			if (rightTitle != null) {
				about.setRightTitle(rightTitle);
			}
			if (rightContent != null) {
				about.setRightContent(rightContent);
			}
			if(visitText != null){
				about.setVisitText(visitText);
			}
			aboutService.saveAbout(about);
		}
		ra.addFlashAttribute("success", "Update succesfully!");
		return "redirect:/admin/about";
	}
}
