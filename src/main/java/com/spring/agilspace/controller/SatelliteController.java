package com.spring.agilspace.controller;

import java.io.File;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.spring.agilspace.model.Partner;
import com.spring.agilspace.model.Satellite;
import com.spring.agilspace.service.PartnerService;
import com.spring.agilspace.service.SatelliteService;
import com.spring.agilspace.util.Constants;
import com.spring.agilspace.util.FileUtils;

@Controller
public class SatelliteController {
	
	@Autowired
	PartnerService partnerService;
	
	@Autowired
	SatelliteService satelliteService;

	@RequestMapping(value ="/admin/satellite", method = RequestMethod.GET)
	public String satellite(Model model, @ModelAttribute("success") String success){
		List<Partner> lsPartner = partnerService.getPartner();
		if(lsPartner != null && !lsPartner.isEmpty()){
			model.addAttribute("lsPartner", lsPartner);
		}
		if(!success.isEmpty()){
			model.addAttribute("success", success);
		}
		return "admin/satellite";
	}
	
	@RequestMapping(value = "admin/searchsatellite", method = RequestMethod.GET)
	public String search(Model model,
//			@RequestParam(value = "partner", required = false) int id,
			@RequestParam(value = "id", required = false) int id,
			 @ModelAttribute("success") String success){
		List<Partner> lsPartner = partnerService.getPartner();
		if(lsPartner != null && !lsPartner.isEmpty()){
			model.addAttribute("lsPartner", lsPartner);
		}
//		Satellite satellite = satelliteService.searchSatellite(id, name);
		Satellite satellite = satelliteService.findSatellite(id);
		Partner partner = partnerService.findPartner(satellite.getPartnerid());
		int displayType = partner.getDisplayType();
		if(satellite != null){
			model.addAttribute("satellitesearch", satellite);
			model.addAttribute("displaytype", displayType);
		}
		if(!success.isEmpty()){
			model.addAttribute("success", success);
		}
		return "admin/satellite";
	}
	@RequestMapping(value = "admin/satellite/{partnerid}", method = RequestMethod.GET)
	public String searchSatellite(Model model, @PathVariable("partnerid") int partnerid){
		Partner partner  = partnerService.findPartner(partnerid);
		model.addAttribute("name", partner.getName());
		List<Satellite> lsSatellite = satelliteService.getByPartner(partnerid);
		model.addAttribute("lsSatellite", lsSatellite);
		return  "/admin/listsatellites";
	}
	@RequestMapping(value = "admin/updatesatellite",params = "update", method = RequestMethod.POST)
	public String editSatellite(Model model,@RequestParam("id") int id, 
			@RequestParam(value = "nameedit", required = false) String name,
			@RequestParam(value = "imageedit", required = false) MultipartFile image,
			@RequestParam(value = "descriptionedit", required = false) String description,
			@RequestParam(value = "description1edit", required = false) String description1,
			@RequestParam(value = "description2edit", required = false) String description2,
			@RequestParam(value = "description3edit", required = false) String description3,
			@RequestParam(value = "featuresedit", required = false) String features,
			@RequestParam(value = "introductionedit",  required = false) String introduction,
			@RequestParam(value = "titleedit",  required = false) String title,
			@RequestParam(value = "countryedit",  required = false) String country,
			@RequestParam(value = "typeedit",  required = false) String type,
			@RequestParam(value = "launchedit",  required = false) String launch,
			@RequestParam(value = "orbitedit",  required = false) String orbit,
			@RequestParam(value = "timeedit",  required = false) String time,
			@RequestParam(value = "lifetimeedit",  required = false) String lifetime,
			@RequestParam(value = "agilityedit",  required = false) String agility,
			@RequestParam(value = "spatialedit",  required = false) String spatial,
			@RequestParam(value = "brandedit",  required = false) String brand,
			@RequestParam(value = "swathedit",  required = false) String swath,
			@RequestParam(value = "sensoredit",  required = false) String sensor,
			HttpServletRequest request, RedirectAttributes ra){
		Satellite satellite = satelliteService.findSatellite(id);
		if(name != null){
			if(!name.equals(satellite.getName())){
				Satellite exist = satelliteService.searchSatellite(satellite.getPartnerid(), name);
				if(exist != null){
					ra.addFlashAttribute("error", "Satellite is existed!");
					return "redirect:/admin/satellite";
				}
			}
			satellite.setName(name);
		}
		if(!image.isEmpty()){
			FileUtils.deleteImagePartner(request, satellite.getImage());
			FileUtils.uploadImagePartner(request, image, Constants.FOLDER_SATELLITE);
			satellite.setImage(Constants.FOLDER_SATELLITE.concat(File.separator).concat(image.getOriginalFilename()));
		}
		if(description != null){
			satellite.setDescription(description);
		}
		if(description1 != null){
			satellite.setDescription1(description1);
		}
		if(description2 != null){
			satellite.setDescription2(description2);
		}
		if(description3 != null){
			satellite.setDescription3(description3);
		}
		if(features != null){
			satellite.setFeature(features);
		}
		if(introduction != null){
			satellite.setIntroduction(introduction);
		}
		if(title != null){
			satellite.setTitle(title);
		}
		if(country != null){
			satellite.setCountry(country);
		}
		if(launch != null){
			satellite.setLaunch(launch);
		}
		if(type != null){
			satellite.setType(type);
		}
		if(orbit != null){
			satellite.setOrbit(orbit);
		}
		if(time != null){
			satellite.setTime(time);
		}
		if(lifetime != null){
			satellite.setLifetime(lifetime);
		}
		if(agility != null){
			satellite.setAgility(agility);
		}
		if(spatial != null){
			satellite.setSpatial(spatial);
		}
		if(brand != null){
			satellite.setBrand(brand);
		}
		if(swath != null){
			satellite.setSwath(swath);
		}
		if(sensor != null){
			satellite.setSensor(sensor);
		}
		satelliteService.saveSatellite(satellite);
		ra.addFlashAttribute("success", "Update succesfully!");
		return "redirect:/admin/satellite";
	}
	@RequestMapping(value = "admin/updatesatellite",params = "delete", method = RequestMethod.POST)
	public String deleteSatellite(Model model, @RequestParam("id") int id, 
			@RequestParam(value = "nameedit", required = false) String name,
			@RequestParam(value = "imageedit", required = false) MultipartFile image,
			@RequestParam(value = "descriptionedit", required = false) String description,
			@RequestParam(value = "featuresedit", required = false) String features,
			@RequestParam(value = "introductionedit",  required = false) String introduction,
			HttpServletRequest request, RedirectAttributes ra){
		Satellite deleteSatellite = satelliteService.findSatellite(id);
		FileUtils.deleteImagePartner(request, deleteSatellite.getImage());
		satelliteService.deleteSatellite(deleteSatellite);
		ra.addFlashAttribute("success", "Delete succesfully!");
		return "redirect:/admin/satellite";
	}
	
	@RequestMapping(value = "/admin/addsatellite", method = RequestMethod.POST)
	public String addSatellite(Model model,
			@RequestParam("name") String name,
			@RequestParam("partner") int id,
			@RequestParam(value = "image", required = false) MultipartFile image,
			@RequestParam(value = "description", required = false) String description,
			@RequestParam(value = "description1", required = false) String description1,
			@RequestParam(value = "description2", required = false) String description2,
			@RequestParam(value = "description3", required = false) String description3,
			@RequestParam(value = "features", required = false) String features,
			@RequestParam(value = "introduction",  required = false) String introduction,
			@RequestParam(value = "title",  required = false) String title,
			@RequestParam(value = "country",  required = false) String country,
			@RequestParam(value = "type",  required = false) String type,
			@RequestParam(value = "launch",  required = false) String launch,
			@RequestParam(value = "orbit",  required = false) String orbit,
			@RequestParam(value = "time",  required = false) String time,
			@RequestParam(value = "lifetime",  required = false) String lifetime,
			@RequestParam(value = "agility",  required = false) String agility,
			@RequestParam(value = "spatial",  required = false) String spatial,
			@RequestParam(value = "brand",  required = false) String brand,
			@RequestParam(value = "swath",  required = false) String swath,
			@RequestParam(value = "sensor",  required = false) String sensor,
			HttpServletRequest request, RedirectAttributes ra){
		Satellite satellite = new Satellite();
		Satellite exist = satelliteService.searchSatellite(id, name);
		if(exist != null){
			ra.addFlashAttribute("error", "Satellite is existed!");
			return "redirect:/admin/satellite";
		}
		satellite.setName(name);
		satellite.setPartnerid(id);
		if(!description.isEmpty()){
			satellite.setDescription(description);
		}
		if(!description1.isEmpty()){
			satellite.setDescription1(description1);
		}
		if(!description2.isEmpty()){
			satellite.setDescription2(description2);
		}
		if(!description3.isEmpty()){
			satellite.setDescription3(description3);
		}
		if(!features.isEmpty()){
			satellite.setFeature(features);
		}
		if(!introduction.isEmpty()){
			satellite.setIntroduction(introduction);
		}
		if(!title.isEmpty()){
			satellite.setTitle(title);
		}
		if(!country.isEmpty()){
			satellite.setCountry(country);
		}
		if(!launch.isEmpty()){
			satellite.setLaunch(launch);
		}
		if(!type.isEmpty()){
			satellite.setType(type);
		}
		if(!orbit.isEmpty()){
			satellite.setOrbit(orbit);
		}
		if(!time.isEmpty()){
			satellite.setTime(time);
		}
		if(!lifetime.isEmpty()){
			satellite.setLifetime(lifetime);
		}
		if(!agility.isEmpty()){
			satellite.setAgility(agility);
		}
		if(!spatial.isEmpty()){
			satellite.setSpatial(spatial);
		}
		if(!brand.isEmpty()){
			satellite.setBrand(brand);
		}
		if(!swath.isEmpty()){
			satellite.setSwath(swath);
		}
		if(!sensor.isEmpty()){
			satellite.setSensor(sensor);
		}
		FileUtils.uploadImagePartner(request, image, Constants.FOLDER_SATELLITE);
		satellite.setImage(Constants.FOLDER_SATELLITE.concat(File.separator).concat(image.getOriginalFilename()));
		satelliteService.saveSatellite(satellite);
		ra.addFlashAttribute("success", "Added succesfully!");
		return "redirect:/admin/satellite";
	}
}
