package com.spring.agilspace.controller;

import java.io.File;
import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.spring.agilspace.model.Partner;
import com.spring.agilspace.model.Satellite;
import com.spring.agilspace.service.PartnerService;
import com.spring.agilspace.service.SatelliteService;
import com.spring.agilspace.util.Constants;
import com.spring.agilspace.util.FileUtils;

@Controller
public class PartnerController {

	@Autowired
	PartnerService partnerService;
	
	@Autowired
	SatelliteService satelliteService;
	
	@RequestMapping(value = "/admin/partner", method = RequestMethod.GET)
    public String partner(Model model){
		List<Partner> lsPartners = partnerService.getPartner();
		if(lsPartners != null && !lsPartners.isEmpty()){
			model.addAttribute("lsPartners", lsPartners);
		}
    	return "admin/partner";
    }
	
	@RequestMapping(value = "/admin/updatepartner",params = "delete", method = RequestMethod.POST)
	public String deletePartner(Model model, @RequestParam("id") int id,HttpServletRequest request){
		Partner deletePartner = partnerService.findPartner(id);
		if(deletePartner != null){
			FileUtils.deleteLogoPartner(request, deletePartner.getLogo());
			partnerService.deletePartner(deletePartner);
			List<Satellite> delteSatellite = satelliteService.getByPartner(id);
			for (Satellite satellite : delteSatellite) {
				satelliteService.deleteSatellite(satellite);
				FileUtils.deleteImagePartner(request, satellite.getImage());
			}
		}
		return "redirect:/admin/partner";
	}
	
	@RequestMapping(value = "/admin/updatepartner", params = "edit", method = RequestMethod.POST)
	public String editPartner(Model model, 
			@RequestParam("partneridedit") int id, 
			@RequestParam("partnernameedit") String partnernameedit, 
			@RequestParam(value = "logoedit", required = false) MultipartFile logoedit,
			HttpServletRequest request, RedirectAttributes ra){
		Partner editPartner = partnerService.findPartner(id);
		boolean isUpload = logoedit.isEmpty();
		if(partnernameedit.equals(editPartner.getName())){
			if(!isUpload){
				FileUtils.deleteImagePartner(request, editPartner.getLogo());
				String logoPath = Constants.FOLDER_PARTNER.concat(File.separator).concat(logoedit.getOriginalFilename());
				editPartner.setLogo(logoPath);
				FileUtils.uploadImagePartner(request, logoedit, Constants.FOLDER_PARTNER);
				partnerService.savePartner(editPartner);
			}
		}else {
			//only rename
			if(isUpload){
				editPartner.setName(partnernameedit);
				Partner existPartner = partnerService.findByName(partnernameedit);
				if(existPartner != null){
					ra.addFlashAttribute("error", "This partner is existed!");
					return "redirect:/admin/partner";
				}
				partnerService.savePartner(editPartner);
			}else {
				ra.addFlashAttribute("error", "please delete and create new one");
				return "redirect:/admin/partner";
			}
		}
		editPartner.setName(partnernameedit);
		return "redirect:/admin/partner";
	}
	
	@RequestMapping(value = "/admin/addpartner", method = RequestMethod.POST)
	public String addPartner(Model model, 
			@RequestParam("name") String name,
			@RequestParam("logo") MultipartFile logo,
			@RequestParam("display") String display,
			HttpServletRequest request, RedirectAttributes ra){
		Partner partner = new Partner();
		Partner existPartner = partnerService.findByName(name);
		if(existPartner != null){
			ra.addFlashAttribute("error", "This partner is existed!");
			return "redirect:/admin/partner";
		}
		partner.setName(name);
		String logoPath = Constants.FOLDER_PARTNER.concat(File.separator).concat(logo.getOriginalFilename());
		partner.setLogo(logoPath);
		FileUtils.uploadImagePartner(request, logo, Constants.FOLDER_PARTNER);
		if(display.equalsIgnoreCase("image")){
			partner.setDisplayType(Constants.ONLY_IMAGE);
		}else {
			partner.setDisplayType(Constants.FULL_INFORMATION);
		}
		partnerService.savePartner(partner);
		return "redirect:/admin/partner";
	}
}
