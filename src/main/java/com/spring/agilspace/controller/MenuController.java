package com.spring.agilspace.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.spring.agilspace.model.Menu;
import com.spring.agilspace.service.MenuService;

@Controller
public class MenuController {
	
	@Autowired
	MenuService menuService;
	
	@RequestMapping(value = "/admin/menu", method =  RequestMethod.GET)
	public String menu(Model model){
		List<Menu> lsmenu = menuService.getMenu();
		if(lsmenu != null && !lsmenu.isEmpty()){
			Menu menu = lsmenu.get(0);
			model.addAttribute("menu", menu);
		}
		return "/admin/menu";
	}
	@RequestMapping(value = "/admin/updaptemenu", method =  RequestMethod.POST)
	public String updateMenu(Model model,
			@RequestParam("id") int id,
			@RequestParam("about") String about,@RequestParam("geoportal") String geoportal,
			@RequestParam("geoservice") String geoservices,@RequestParam("geoserviceSub1") String geoserviceSub1,
			@RequestParam("geoserviceSub2") String geoserviceSub2,@RequestParam("geoserviceSub3") String geoserviceSub3,
			@RequestParam("geoserviceSub4") String geoserviceSub4,@RequestParam("satellite") String satellite,
			@RequestParam("sateliteSub1") String sateliteSub1,@RequestParam("sateliteSub2") String sateliteSub2,
			@RequestParam("integerated") String integerated,@RequestParam("serviceSub1") String serviceSub1,
			@RequestParam("serviceSub2") String serviceSub2,@RequestParam("serviceSub3") String serviceSub3,
			@RequestParam("news") String news,@RequestParam("newsSub1") String newsSub1,
			@RequestParam("newsSub2") String newsSub2,@RequestParam("newsSub3") String newsSub3,
			@RequestParam("contact") String contact){
		Menu menu = menuService.findMenu(id);
		menu.setAbout(about);
		menu.setGeoportal(geoportal);
		menu.setGeoservices(geoservices);
		menu.setGeoserviceSub1(geoserviceSub1);
		menu.setGeoserviceSub2(geoserviceSub2);
		menu.setGeoserviceSub3(geoserviceSub3);
		menu.setGeoserviceSub4(geoserviceSub4);
		menu.setSatelite(satellite);
		menu.setSateliteSub1(sateliteSub1);
		menu.setSateliteSub2(sateliteSub2);
		menu.setIntegratedService(integerated);
		menu.setServiceSub1(serviceSub1);
		menu.setServiceSub2(serviceSub2);
		menu.setServiceSub3(serviceSub3);
		menu.setNews(news);
		menu.setNewsSub1(newsSub1);
		menu.setNewsSub2(newsSub2);
		menu.setNewsSub3(newsSub3);
		menu.setContact(contact);
		menuService.saveMenu(menu);
		return "redirect:/admin/menu";
	}
}
