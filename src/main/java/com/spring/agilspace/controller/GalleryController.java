package com.spring.agilspace.controller;

import java.io.File;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import com.spring.agilspace.model.Gallery;
import com.spring.agilspace.service.GalleryService;
import com.spring.agilspace.util.Constants;
import com.spring.agilspace.util.FileUtils;

@Controller
public class GalleryController {
	
	@Autowired
	GalleryService galleryService;

	@RequestMapping(value = "/admin/gallery", method = RequestMethod.GET)
	public String  gallery(Model model){
		List<Gallery> lsGallery = galleryService.getGallery();
		if(!lsGallery.isEmpty()){
			model.addAttribute("lsGallery", lsGallery);
		}
		return "/admin/gallery";
	}
	
	@RequestMapping(value = "/admin/addgallery", method = RequestMethod.POST)
	public String addGallery(Model model,
			@RequestParam("title") String title,
			@RequestParam("image") MultipartFile image, HttpServletRequest request){
		Gallery gallery = new Gallery();
		gallery.setTitle(title);
		String imagePath = Constants.FOLDER_GALLERY.concat(File.separator).concat(image.getOriginalFilename());
		gallery.setImage(imagePath);
		FileUtils.uploadGallery(request, image);
		galleryService.saveGallery(gallery);
		return "redirect:/admin/gallery";
	}
	@RequestMapping(value = "/admin/updategallery",params = "update", method = RequestMethod.POST)
	public String updateGallery(Model model,
			@RequestParam("id") int id,
			@RequestParam("edittitle") String title,
			@RequestParam(value = "editimage", required =  false) MultipartFile image,
			HttpServletRequest request){
		Gallery updateGallery = galleryService.findGallery(id);
		updateGallery.setTitle(title);
		if(image.isEmpty()){
			galleryService.saveGallery(updateGallery);
			return "redirect:/admin/gallery";
		}else {
			//change picture
			FileUtils.deleteGallery(request, updateGallery.getImage());
			String imagePath = Constants.FOLDER_GALLERY.concat(File.separator).concat(image.getOriginalFilename());
			FileUtils.uploadGallery(request, image);
			updateGallery.setImage(imagePath);
			galleryService.saveGallery(updateGallery);
			return "redirect:/admin/gallery";
		}
	}

	@RequestMapping(value = "/admin/updategallery", params = "delete", method = RequestMethod.POST)
	public String deleteGallery(
			Model model,
			@RequestParam("id") int id,
			@RequestParam("edittitle") String title,
			@RequestParam(value = "editimage", required = false) MultipartFile image,
			HttpServletRequest request) {
		Gallery deleteGallery = galleryService.findGallery(id);
		FileUtils.deleteGallery(request, deleteGallery.getImage());
		galleryService.deleteGallery(deleteGallery);
		return "redirect:/admin/gallery";
	}
}
