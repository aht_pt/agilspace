package com.spring.agilspace.controller;

import java.io.File;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.spring.agilspace.model.Contact;
import com.spring.agilspace.model.Home;
import com.spring.agilspace.model.SMTPEmail;
import com.spring.agilspace.model.User;
import com.spring.agilspace.service.ContactService;
import com.spring.agilspace.service.HomeService;
import com.spring.agilspace.service.SMTPService;
import com.spring.agilspace.service.UserService;
import com.spring.agilspace.util.Constants;
import com.spring.agilspace.util.FileUtils;


@Controller
public class AdminController {

	@Autowired 
	UserService userService;
	
	@Autowired
	BCryptPasswordEncoder bCryptPasswordEncoder;
	
	@Autowired
	ContactService contactService;
	
	@Autowired
	SMTPService smtpService;
	
	@Autowired
	HomeService homeService;
	
	@GetMapping(value = {"/admin", "/admin/index"})
	public String adminIndex(Model model,
			@ModelAttribute("error") String error,
			@ModelAttribute("success") String success) {
		if(!error.isEmpty()){
			model.addAttribute("error", error);
		}
		if(!success.isEmpty()){
			model.addAttribute("success", success);
		}
		List<Contact> lsContact = contactService.getContact();
		if(lsContact != null && !lsContact.isEmpty()){
			Contact contact = lsContact.get(0);
			model.addAttribute("contact", contact);
		}
		
		List<SMTPEmail> lsSMTP = smtpService.getEmail();
		if(lsSMTP != null && !lsSMTP.isEmpty()){
			SMTPEmail smtpEmail = lsSMTP.get(0);
			model.addAttribute("SMTPEmail", smtpEmail);
		}
        return "admin/index";
    }
	
	@RequestMapping(value = "/admin/home", method = RequestMethod.GET)
	public String home(Model model){
		List<Home> lsHome = homeService.getHome();
		if(!lsHome.isEmpty()){
			Home home = lsHome.get(0);
			model.addAttribute("home", home);
		}
		return "admin/home";
	}
	
	@RequestMapping(value = "/admin/updaptehome", method = RequestMethod.POST)
	public String updateHome(Model model,
			@RequestParam("appName") String appName,
			@RequestParam("explore") String explore,
			@RequestParam("start") String start,
			@RequestParam("singaportintro") String singaportintro,
			@RequestParam(name = "background1", required = false) MultipartFile background1,
			@RequestParam(name = "background2", required = false) MultipartFile background2,
			@RequestParam("introduction") String introduction, HttpServletRequest request) {
		List<Home> lsHome = homeService.getHome();
		if(!lsHome.isEmpty()){
			Home home = lsHome.get(0);
			home.setAppName(appName);
			home.setExplore(explore);
			home.setStart(start);
			home.setIntroduction(introduction);
			home.setSingaportintro(singaportintro);
			if(!background1.isEmpty()){
				FileUtils.uploadBackground(request, background1, Constants.BACKGROUND_FILE_1);
				String imagePath1 = Constants.FOLDER_IMAGE.concat(File.separator).concat(Constants.BACKGROUND_FILE_1);
				home.setBackground1(imagePath1);
			}
			if(!background2.isEmpty()){
				FileUtils.uploadBackground(request, background2, Constants.BACKGROUND_FILE_2);
				String imagePath2 = Constants.FOLDER_IMAGE.concat(File.separator).concat(Constants.BACKGROUND_FILE_2);
				home.setBackground2(imagePath2);
			}
			homeService.saveHome(home);
		}
		return "redirect:/admin/home";
	}
	
	@RequestMapping(value = "/admin/updatecontact", method = RequestMethod.POST)
	public String updateContact(Model model,
			@RequestParam(value = "title", required = false) String title,
			@RequestParam(value = "phone", required = false) String phone,
			@RequestParam(value = "email", required = false) String email,
			@RequestParam(value = "send", required = false) String send,
			@RequestParam(value = "address", required = false) String address,
			@RequestParam(value = "electronics", required = false) String electronics,
			@RequestParam(value = "nameform", required = false) String nameform,
			@RequestParam(value = "emailform", required = false) String emailform,
			@RequestParam(value = "contactform", required = false) String contactform,
			@RequestParam(value = "companyform", required = false) String companyform,
			@RequestParam(value = "enquiry", required = false) String enquiry,
			@RequestParam(value = "enquiryoption", required = false) String enquiryoption,
			@RequestParam(value = "message", required = false) String message){
		
		List<Contact> lsContact = contactService.getContact();
		if(lsContact != null && !lsContact.isEmpty()){
			Contact contact = lsContact.get(0);
			if(title != null){
				contact.setPageTitle(title);
			}
			if(phone != null){
				contact.setPhoneNumber(phone);
			}
			if(email != null){
				contact.setEmailAddress(email);
			}
			if(send  != null){
				contact.setSend(send);
			}
			if(address != null){
				contact.setAddress(address);
			}
			if(electronics != null){
				contact.setElectronics(electronics);
			}
			if(nameform != null){
				contact.setNameform(nameform);
			}
			if(emailform != null){
				contact.setEmailform(emailform);
			}
			if(contactform != null){
				contact.setContactform(contactform);
			}
			if(companyform != null){
				contact.setCompanyform(companyform);
			}
			if(enquiry != null){
				contact.setEnquiry(enquiry);
			}
			if(enquiryoption != null){
				contact.setEnquiryoption(enquiryoption);
			}
			if(message != null){
				contact.setMessage(message);
			}
			model.addAttribute("contact", contact);
			contactService.saveContact(contact);
		}
		return "redirect:/admin/index";
	}
	
	@RequestMapping(value ="/admin/changepassword", method = RequestMethod.POST)
	public String changepassword(Model model,
			@RequestParam(value = "password", required = false) String password, 
			@RequestParam(value = "newpassword", required = false) String newpassword,
			@RequestParam(value = "confirmpassword", required = false) String confirmpassword,
			RedirectAttributes ra){
		User admin = userService.findbyUsername("admin");
		boolean matches = bCryptPasswordEncoder.matches(password, admin.getPassword());
		if(!matches){
			ra.addFlashAttribute("error", "Current password is not match!");
			return "redirect:/admin/index";
		}else {
			if(!newpassword.equals(confirmpassword)){
				ra.addFlashAttribute("error", "Confirm password is not match!");
				return "redirect:/admin/index";
			}
			if(newpassword.length() >16){
				ra.addFlashAttribute("error", "Max length password is 16 characters!");
				return "redirect:/admin/index";
			}
			admin.setPassword(newpassword);
			userService.saveUser(admin);
			ra.addFlashAttribute("success", "Change password successfully!");
			return "redirect:/admin/index";
		}
	}
}
