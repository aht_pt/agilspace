package com.spring.agilspace.controller;

import java.io.IOException;
import java.util.Date;
import java.util.List;
import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.spring.agilspace.model.Contact;
import com.spring.agilspace.model.SMTPEmail;
import com.spring.agilspace.model.User;
import com.spring.agilspace.service.ContactService;
import com.spring.agilspace.service.SMTPService;
import com.spring.agilspace.service.UserService;

@Controller
public class ContactController {

	@Autowired
	ContactService contactService;
	
	@Autowired
	SMTPService smtpService;
	
	@Autowired
	UserService userService;
	
	@RequestMapping(value = "/sendemail", method = RequestMethod.POST)
	public String sendEmail(Model model,
			@RequestParam("nameform") String nameform,
			@RequestParam("emailform") String emailform,
			@RequestParam("contactform") String contactform,
			@RequestParam("companyform") String companyform,
			@RequestParam("enquiry") String enquiry,
			@RequestParam("message") String message, RedirectAttributes ra)
			throws AddressException, MessagingException, IOException {
		boolean success = false;
		success = sendMail(nameform, emailform, contactform, companyform, enquiry, message);
		if(success){
			ra.addFlashAttribute("message", "You message has been sent!");
		}
		return "redirect:/contact-us";
	}
	
	@RequestMapping(value = "/admin/smtpemail", method = RequestMethod.POST)
	public String updateSMTP(Model model,
			@RequestParam("smtpemail") String smtpemail,
			@RequestParam("smtppassword") String smtppassword,
			RedirectAttributes ra) {
		List<SMTPEmail> lsEmail = smtpService.getEmail();
		if(!lsEmail.isEmpty()){
			SMTPEmail smtpEmail = lsEmail.get(0);
			smtpEmail.setEmail(smtpemail);
			smtpEmail.setPassword(smtppassword);
			smtpService.saveEmail(smtpEmail);
		}
		return "redirect:/admin/index";
	}
	
	public boolean sendMail(String nameform, String emailform, String contactform,
			String companyform, String enquiry, String message)
			throws AddressException, MessagingException, IOException {
		Properties props = new Properties();
		props.put("mail.smtp.auth", "true");
		props.put("mail.smtp.starttls.enable", "true");
		props.put("mail.smtp.host", "smtp.gmail.com");
		props.put("mail.smtp.port", "587");
		SMTPEmail smtpEmail = smtpService.getEmail().get(0);
		String email = smtpEmail.getEmail();
		String password = smtpEmail.getPassword();
		List<Contact> lsContact = contactService.getContact();
		String emailTo = lsContact.get(0).getEmailAddress();
		Session session = Session.getInstance(props,
				new javax.mail.Authenticator() {
					protected PasswordAuthentication getPasswordAuthentication() {
						return new PasswordAuthentication(
								email, password);
					}
				});
		Message msg = new MimeMessage(session);
		msg.setFrom(new InternetAddress(email, false));

		msg.setRecipients(Message.RecipientType.TO,
				InternetAddress.parse(emailTo));
		String subject = "Sent from email address ".concat(emailform);
		msg.setSubject(subject);
		msg.setSentDate(new Date());

		MimeBodyPart bodyPartName = new MimeBodyPart();
		bodyPartName.setContent("Name: " +nameform + " <br>", "text/html");
		
		MimeBodyPart bodyPartEmail = new MimeBodyPart();
		bodyPartEmail.setContent("Email: " +emailform + " <br>", "text/html");
		
		MimeBodyPart bodyPartNumber = new MimeBodyPart();
		bodyPartNumber.setContent("Contact Number: " + contactform + " <br>", "text/html");
		
		MimeBodyPart bodyPartCompany = new MimeBodyPart();
		bodyPartCompany.setContent("Company Name: " +companyform + " <br>", "text/html");
		
		MimeBodyPart bodyPartEnquiry = new MimeBodyPart();
		bodyPartEnquiry.setContent("Enquiry: " + enquiry + " <br>", "text/html");
		
		MimeBodyPart bodyPartMessage = new MimeBodyPart();
		bodyPartMessage.setContent("Message: " +message + " <br>", "text/html");

		Multipart multipart = new MimeMultipart();
		multipart.addBodyPart(bodyPartName);
		multipart.addBodyPart(bodyPartEmail);
		multipart.addBodyPart(bodyPartNumber);
		multipart.addBodyPart(bodyPartCompany);
		multipart.addBodyPart(bodyPartEnquiry);
		multipart.addBodyPart(bodyPartMessage);
//		MimeBodyPart attachPart = new MimeBodyPart();
//
//		attachPart.attachFile("/var/tmp/image19.png");
//		multipart.addBodyPart(attachPart);
		msg.setContent(multipart);
		Transport.send(msg);
		return true;
	}
}
