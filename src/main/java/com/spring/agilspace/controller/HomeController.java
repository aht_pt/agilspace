package com.spring.agilspace.controller;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.spring.agilspace.model.About;
import com.spring.agilspace.model.Contact;
import com.spring.agilspace.model.Gallery;
import com.spring.agilspace.model.Home;
import com.spring.agilspace.model.Menu;
import com.spring.agilspace.model.Partner;
import com.spring.agilspace.model.Satellite;
import com.spring.agilspace.model.SatelliteImage;
import com.spring.agilspace.service.AboutService;
import com.spring.agilspace.service.ContactService;
import com.spring.agilspace.service.GalleryService;
import com.spring.agilspace.service.HomeService;
import com.spring.agilspace.service.MenuService;
import com.spring.agilspace.service.PartnerService;
import com.spring.agilspace.service.SatelliteService;
import com.spring.agilspace.util.Constants;

@Controller
public class HomeController {
	
	@Autowired
	MenuService menuService;
	
	@Autowired
	ContactService contactService;
	
	@Autowired
	PartnerService partnerService;
	
	@Autowired
	SatelliteService satelliteService;
	
	@Autowired
	GalleryService galleryService;
	
	@Autowired
	AboutService aboutService;
	
	@Autowired
	HomeService homeService;

    @GetMapping("/")
    public String root(Model model, @ModelAttribute("checkIndex") String checkIndex) {
    	Menu menu = menuService.getMenu().get(0);
    	if(menu != null){
    		model.addAttribute("menu", menu);
    	}
    	List<Partner> lsPartner = partnerService.getPartner();
    	Partner pSingapore = null;
    	if(lsPartner != null && !lsPartner.isEmpty()){
    		
    		for (Iterator iterator = lsPartner.iterator(); iterator.hasNext();) {
				Partner partner = (Partner) iterator.next();
				if(partner.getName().contains("Singapore")){
					pSingapore = partner;
					iterator.remove();
				}
			}
    		model.addAttribute("lsPartner", lsPartner);
    	}
    	List<Home> lsHome = homeService.getHome();
    	if(!lsHome.isEmpty()){
    		Home home = lsHome.get(0);
    		model.addAttribute("home", home);
    	}
    	if(pSingapore != null){
    		List<Satellite> satelliteSingapore = satelliteService.getByPartner(pSingapore.getId());
    		if(!satelliteSingapore.isEmpty()){
    			long random = Math.round((Math.random() + 2) * 100);
    			model.addAttribute("SingaporeSatellite", satelliteSingapore);
    			model.addAttribute("random", random);
    		}
    	}
		model.addAttribute("Singapore", pSingapore);
		model.addAttribute("checkIndex", checkIndex);
        return "index";
    }

    @GetMapping("/user")
    public String userIndex() {
        return "user/index";
    }
    
    @RequestMapping(value = "/about-us", method = RequestMethod.GET)
    public String aboutus(Model model){
    	Menu menu = menuService.getMenu().get(0);
    	if(menu != null){
    		model.addAttribute("menu", menu);
    	}
    	List<About> lsAbout = aboutService.getAbout();
    	if(!lsAbout.isEmpty()){
    		model.addAttribute("about", lsAbout.get(0));
    	}
    	return "aboutus";
    }
    
    @RequestMapping(value = "/partner/{name}", method = RequestMethod.GET)
    public String partner(Model model, @PathVariable("name") String name){
    	Menu menu = menuService.getMenu().get(0);
    	if(menu != null){
    		model.addAttribute("menu", menu);
    	}
    	List<Partner> lsPartner = partnerService.getPartner();
    	if(lsPartner != null && !lsPartner.isEmpty()){
    		
    		for (Iterator iterator = lsPartner.iterator(); iterator.hasNext();) {
				Partner partner = (Partner) iterator.next();
				if(partner.getName().contains("Singapore")){
					iterator.remove();
				}
			}
    		model.addAttribute("lsPartner", lsPartner);
    	}
    	Partner partner = partnerService.findByName(name);
		if (partner != null) {
			List<Satellite> lsSatellite = satelliteService.getByPartner(partner
					.getId());
			if (!lsSatellite.isEmpty()) {
				model.addAttribute("lsSatellite", lsSatellite);

				List<SatelliteImage> lsSatelliteImage = new ArrayList<SatelliteImage>();
				List<Satellite> lsEvenSatellite = new ArrayList<Satellite>();
				for (int i = 0; i < lsSatellite.size(); i++) {
					lsEvenSatellite.add(lsSatellite.get(i));
					// set row for section full page
					if (lsEvenSatellite.size() == 2
							|| i == lsSatellite.size() - 1) {
						SatelliteImage satelliteImage = new SatelliteImage();
						satelliteImage.setListSatellite(lsEvenSatellite);
						satelliteImage
								.setRandom(Math.round((Math.random() + 2) * 100));
						lsSatelliteImage.add(satelliteImage);
						lsEvenSatellite = new ArrayList<Satellite>();
					}
				}
				model.addAttribute("lsSatelliteImage", lsSatelliteImage);
				model.addAttribute("name", name);
				if (Constants.ONLY_IMAGE == partner.getDisplayType()) {
					return "partnerhome";
				} else if (Constants.FULL_INFORMATION == partner
						.getDisplayType()) {
					return "fullstats";
				}
			}
		}
		model.addAttribute("name", name);
		return "partnerhome";
	}
    
    @RequestMapping(value = "/satellite/{id}", method = RequestMethod.GET)
    public String satellite(Model model,@PathVariable("id") String id){
    	Menu menu = menuService.getMenu().get(0);
    	if(menu != null){
    		model.addAttribute("menu", menu);
    	}
    	List<Gallery> lsGallery = galleryService.getGallery();
    	if(!lsGallery.isEmpty()){
    		model.addAttribute("lsGallery", lsGallery);
    		if(lsGallery.size() >= 14){
    			model.addAttribute("sizeGallery", lsGallery.size() - 14);
    		}
    	}
    	int hyphen = id.lastIndexOf(Constants.HYPHEN);
        String encodeId = id.substring(hyphen+1, id.length());
        int satelliteId = Integer.valueOf(encodeId.substring(3, encodeId.length()));
        Satellite satellite = satelliteService.findSatellite(satelliteId);
        if(satellite != null){
        	model.addAttribute("satellite", satellite);
        	int partnerid = satellite.getPartnerid();
        	Partner partner = partnerService.findPartner(partnerid);
        	model.addAttribute("partnername", partner.getName());
        }
    	return "pagedetail";
    }
    
    @GetMapping("/admin/login/{alias}")
    public String login() {
        return "login";
    }

    @GetMapping("/admin/forgot")
    public String forgot() {
        return "forgot";
    }

    @RequestMapping(value = "/gallery", method = RequestMethod.GET)
    public String gallery(Model model){
    	Menu menu = menuService.getMenu().get(0);
    	if(menu != null){
    		model.addAttribute("menu", menu);
    	}
    	List<Gallery> lsGallery = galleryService.getGallery();
    	if(!lsGallery.isEmpty()){
    		model.addAttribute("lsGallery", lsGallery);
    	}
    	return "galleryhome";
    }

    @RequestMapping(value = "/contact-us", method = RequestMethod.GET)
    public String contact(Model model, @ModelAttribute("message") String message){
    	Menu menu = menuService.getMenu().get(0);
    	if(menu != null){
    		model.addAttribute("menu", menu);
    	}
    	Contact contact = contactService.getContact().get(0);
    	if(contact != null){
    		model.addAttribute("contact", contact);
    		if(!contact.getEnquiryoption().isEmpty()){
    			List<String> lsEnquiryoption = new ArrayList<String>();
    			if(contact.getEnquiryoption().contains(Constants.COMMA)){
    				String[] options =  contact.getEnquiryoption().split(Constants.COMMA);
    				for (String option : options) {
    					lsEnquiryoption.add(option);
    				}
    			}else {
    				lsEnquiryoption.add(contact.getEnquiryoption());
    			}
    			model.addAttribute("lsEnquiryoption", lsEnquiryoption);
    		}
    	}
    	if(!message.isEmpty()){
    		model.addAttribute("message", message);
    	}
    	return "contactus";
    }

    @GetMapping("/access-denied")
    public String accessDenied() {
        return "/error/access-denied";
    }
}
