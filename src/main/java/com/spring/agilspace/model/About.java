package com.spring.agilspace.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name ="About")
public class About {

	@Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id", nullable = false)
    private int id;
	
	@Column(name = "title")
	private String title;
	
	@Column(name = "left_title")
	private String leftTitle;
	
	@Column(name = "left_content", length = 10000)
	private String leftContent;
	
	@Column(name = "right_title")
	private String rightTitle;
	
	@Column(name = "right_content", length = 10000)
	private String rightContent;
	
	@Column(name = "visit_text")
	private String visitText;
	

	public About() {
		super();
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getLeftTitle() {
		return leftTitle;
	}

	public void setLeftTitle(String leftTitle) {
		this.leftTitle = leftTitle;
	}

	public String getLeftContent() {
		return leftContent;
	}

	public void setLeftContent(String leftContent) {
		this.leftContent = leftContent;
	}

	public String getRightTitle() {
		return rightTitle;
	}

	public void setRightTitle(String rightTitle) {
		this.rightTitle = rightTitle;
	}

	public String getRightContent() {
		return rightContent;
	}

	public void setRightContent(String rightContent) {
		this.rightContent = rightContent;
	}

	public String getVisitText() {
		return visitText;
	}

	public void setVisitText(String visitText) {
		this.visitText = visitText;
	}
	
	
}
