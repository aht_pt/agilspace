package com.spring.agilspace.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name ="Satellite")
public class Satellite {

	@Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id", nullable = false)
    private int id;
	
	@Column(name = "name")
	private String name;
	
	@Column(name = "image")
	private String image;
	
	@Column(name ="features", length = 10000)
	private String feature;
	
	@Column(name = "description", length = 10000)
	private String description;
	
	@Column(name = "description1", length = 10000)
	private String description1;
	
	@Column(name = "description2", length = 10000)
	private String description2;
	
	@Column(name = "description3", length = 10000)
	private String description3;
	
	@Column(name = "introduction", length = 10000)
	private String introduction;
	
	@Column(name = "partnerid")
	private int partnerid;
	
	@Column(name = "title")
	private String title;
	
	@Column(name = "country")
	private String country;
	
	@Column(name = "type")
	private String type;
	
	@Column(name = "time")
	private String time;
	
	@Column(name = "launch")
	private String launch;
	
	@Column(name = "orbit")
	private String orbit;
	
	@Column(name = "lifetime")
	private String lifetime;
	
	@Column(name = "agility")
	private String agility;
	
	@Column(name = "spatial")
	private String spatial;
	
	@Column(name = "brand")
	private String brand;
	
	@Column(name = "swath")
	private String swath;
	
	@Column(name = "sensor")
	private String sensor;
	
	public Satellite() {
		super();
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public String getFeature() {
		return feature;
	}

	public void setFeature(String feature) {
		this.feature = feature;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}


	public String getIntroduction() {
		return introduction;
	}

	public void setIntroduction(String introduction) {
		this.introduction = introduction;
	}


	public int getPartnerid() {
		return partnerid;
	}

	public void setPartnerid(int partnerid) {
		this.partnerid = partnerid;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getTime() {
		return time;
	}

	public void setTime(String time) {
		this.time = time;
	}

	public String getLifetime() {
		return lifetime;
	}

	public void setLifetime(String lifetime) {
		this.lifetime = lifetime;
	}

	public String getAgility() {
		return agility;
	}

	public void setAgility(String agility) {
		this.agility = agility;
	}

	public String getSpatial() {
		return spatial;
	}

	public void setSpatial(String spatial) {
		this.spatial = spatial;
	}

	public String getBrand() {
		return brand;
	}

	public void setBrand(String brand) {
		this.brand = brand;
	}

	public String getSwath() {
		return swath;
	}

	public void setSwath(String swath) {
		this.swath = swath;
	}

	public String getSensor() {
		return sensor;
	}

	public void setSensor(String sensor) {
		this.sensor = sensor;
	}

	public String getLaunch() {
		return launch;
	}

	public void setLaunch(String launch) {
		this.launch = launch;
	}

	public String getOrbit() {
		return orbit;
	}

	public void setOrbit(String orbit) {
		this.orbit = orbit;
	}

	public String getDescription1() {
		return description1;
	}

	public void setDescription1(String description1) {
		this.description1 = description1;
	}

	public String getDescription2() {
		return description2;
	}

	public void setDescription2(String description2) {
		this.description2 = description2;
	}

	public String getDescription3() {
		return description3;
	}

	public void setDescription3(String description3) {
		this.description3 = description3;
	}
}
