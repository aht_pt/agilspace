package com.spring.agilspace.model;

import java.util.List;

public class SatelliteImage {

	List<Satellite> listSatellite;
	
	long random;

	public List<Satellite> getListSatellite() {
		return listSatellite;
	}

	public void setListSatellite(List<Satellite> listSatellite) {
		this.listSatellite = listSatellite;
	}

	public long getRandom() {
		return random;
	}

	public void setRandom(long random) {
		this.random = random;
	}
	
}
