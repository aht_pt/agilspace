package com.spring.agilspace.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "Home")
public class Home {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id", nullable = false)
	private int id;
	
	@Column(name = "app_name")
	private String appName;
	
	@Column(name = "introduction")
	private String introduction;
	
	@Column(name = "explore")
	private String explore;
	
	@Column(name = "start")
	private String start;
	
	@Column(name = "singaportintro")
	private String singaportintro;
	
	@Column(name = "background1")
	private String background1;
	
	@Column(name = "background2")
	private String background2;
	
	public Home() {
		super();
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getAppName() {
		return appName;
	}

	public void setAppName(String appName) {
		this.appName = appName;
	}

	public String getIntroduction() {
		return introduction;
	}

	public void setIntroduction(String introduction) {
		this.introduction = introduction;
	}

	public String getExplore() {
		return explore;
	}

	public void setExplore(String explore) {
		this.explore = explore;
	}

	public String getStart() {
		return start;
	}

	public void setStart(String start) {
		this.start = start;
	}

	public String getSingaportintro() {
		return singaportintro;
	}

	public void setSingaportintro(String singaportintro) {
		this.singaportintro = singaportintro;
	}

	public String getBackground1() {
		return background1;
	}

	public void setBackground1(String background1) {
		this.background1 = background1;
	}

	public String getBackground2() {
		return background2;
	}

	public void setBackground2(String background2) {
		this.background2 = background2;
	}

}
