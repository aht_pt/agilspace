package com.spring.agilspace.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name ="Menu")
public class Menu {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id", nullable = false)
	private int id;
	
	@Column(name = "about")
	private String about;
	
	@Column(name = "geoportal")
	private String geoportal;
	
	@Column(name = "geoservices")
	private String geoservices;
	
	@Column(name = "geoservices_sub1")
	private String geoserviceSub1;
	
	@Column(name = "geoservices_sub2")
	private String geoserviceSub2;
	
	@Column(name = "geoservices_sub3")
	private String geoserviceSub3;
	
	@Column(name = "geoservices_sub4")
	private String geoserviceSub4;
	
	@Column(name = "satelite")
	private String satelite;
	
	@Column(name = "satelite_sub1")
	private String sateliteSub1;
	
	@Column(name = "satelite_sub2")
	private String sateliteSub2;
	
	@Column(name = "integrated_service")
	private String integratedService;
	
	@Column(name = "service_sub1")
	private String serviceSub1;
	
	@Column(name = "service_sub2")
	private String serviceSub2;
	
	@Column(name = "service_sub3")
	private String serviceSub3;
	
	@Column(name = "news")
	private String news;
	
	@Column(name = "news_sub1")
	private String newsSub1;
	
	@Column(name = "news_sub2")
	private String newsSub2;
	
	@Column(name = "news_sub3")
	private String newsSub3;
	
	@Column(name = "contact")
	private String contact;
	

	public Menu() {
		super();
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getAbout() {
		return about;
	}

	public void setAbout(String about) {
		this.about = about;
	}

	public String getGeoportal() {
		return geoportal;
	}

	public void setGeoportal(String geoportal) {
		this.geoportal = geoportal;
	}

	public String getGeoservices() {
		return geoservices;
	}

	public void setGeoservices(String geoservices) {
		this.geoservices = geoservices;
	}

	public String getGeoserviceSub1() {
		return geoserviceSub1;
	}

	public void setGeoserviceSub1(String geoserviceSub1) {
		this.geoserviceSub1 = geoserviceSub1;
	}

	public String getGeoserviceSub2() {
		return geoserviceSub2;
	}

	public void setGeoserviceSub2(String geoserviceSub2) {
		this.geoserviceSub2 = geoserviceSub2;
	}

	public String getGeoserviceSub3() {
		return geoserviceSub3;
	}

	public void setGeoserviceSub3(String geoserviceSub3) {
		this.geoserviceSub3 = geoserviceSub3;
	}

	public String getGeoserviceSub4() {
		return geoserviceSub4;
	}

	public void setGeoserviceSub4(String geoserviceSub4) {
		this.geoserviceSub4 = geoserviceSub4;
	}

	public String getSatelite() {
		return satelite;
	}

	public void setSatelite(String satelite) {
		this.satelite = satelite;
	}

	public String getSateliteSub1() {
		return sateliteSub1;
	}

	public void setSateliteSub1(String sateliteSub1) {
		this.sateliteSub1 = sateliteSub1;
	}

	public String getSateliteSub2() {
		return sateliteSub2;
	}

	public void setSateliteSub2(String sateliteSub2) {
		this.sateliteSub2 = sateliteSub2;
	}

	public String getIntegratedService() {
		return integratedService;
	}

	public void setIntegratedService(String integratedService) {
		this.integratedService = integratedService;
	}

	public String getServiceSub1() {
		return serviceSub1;
	}

	public void setServiceSub1(String serviceSub1) {
		this.serviceSub1 = serviceSub1;
	}

	public String getServiceSub2() {
		return serviceSub2;
	}

	public void setServiceSub2(String serviceSub2) {
		this.serviceSub2 = serviceSub2;
	}

	public String getServiceSub3() {
		return serviceSub3;
	}

	public void setServiceSub3(String serviceSub3) {
		this.serviceSub3 = serviceSub3;
	}

	public String getNews() {
		return news;
	}

	public void setNews(String news) {
		this.news = news;
	}

	public String getNewsSub1() {
		return newsSub1;
	}

	public void setNewsSub1(String newsSub1) {
		this.newsSub1 = newsSub1;
	}

	public String getNewsSub2() {
		return newsSub2;
	}

	public void setNewsSub2(String newsSub2) {
		this.newsSub2 = newsSub2;
	}

	public String getNewsSub3() {
		return newsSub3;
	}

	public void setNewsSub3(String newsSub3) {
		this.newsSub3 = newsSub3;
	}

	public String getContact() {
		return contact;
	}

	public void setContact(String contact) {
		this.contact = contact;
	}
	
}
