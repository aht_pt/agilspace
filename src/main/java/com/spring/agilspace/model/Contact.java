package com.spring.agilspace.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="Contact")
public class Contact {
	
	@Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id", nullable = false)
    private int id;
	
	@Column(name ="page_title")
	private String pageTitle;
	
	@Column(name ="phone_number")
	private String phoneNumber;
	
	@Column(name ="email_address")
	private String emailAddress;
	
	@Column(name="address")
	private String address;
	
	@Column(name ="send")
	private String send;
	
	@Column(name = "electronics")
	private String electronics;
	
	@Column(name = "nameform")
	private String nameform;
	
	@Column(name = "emailform")
	private String emailform;
	
	@Column(name ="contactform")
	private String contactform;
	
	@Column(name = "companyform")
	private String companyform;
	
	@Column(name = "enquiry")
	private String enquiry;
	
	@Column(name = "enquiryoption")
	private String enquiryoption;
	
	@Column(name = "message")
	private String message;

	public Contact() {
		super();
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getPageTitle() {
		return pageTitle;
	}

	public void setPageTitle(String pageTitle) {
		this.pageTitle = pageTitle;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getEmailAddress() {
		return emailAddress;
	}

	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getSend() {
		return send;
	}

	public void setSend(String send) {
		this.send = send;
	}

	public String getElectronics() {
		return electronics;
	}

	public void setElectronics(String electronics) {
		this.electronics = electronics;
	}

	public String getNameform() {
		return nameform;
	}

	public void setNameform(String nameform) {
		this.nameform = nameform;
	}

	public String getEmailform() {
		return emailform;
	}

	public void setEmailform(String emailform) {
		this.emailform = emailform;
	}

	public String getContactform() {
		return contactform;
	}

	public void setContactform(String contactform) {
		this.contactform = contactform;
	}

	public String getCompanyform() {
		return companyform;
	}

	public void setCompanyform(String companyform) {
		this.companyform = companyform;
	}

	public String getEnquiry() {
		return enquiry;
	}

	public void setEnquiry(String enquiry) {
		this.enquiry = enquiry;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getEnquiryoption() {
		return enquiryoption;
	}

	public void setEnquiryoption(String enquiryoption) {
		this.enquiryoption = enquiryoption;
	}
	
	
	
	
}
