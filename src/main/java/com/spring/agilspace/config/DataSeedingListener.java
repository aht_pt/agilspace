package com.spring.agilspace.config;

import java.io.File;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;

import com.spring.agilspace.model.Contact;
import com.spring.agilspace.model.Home;
import com.spring.agilspace.model.Menu;
import com.spring.agilspace.model.SMTPEmail;
import com.spring.agilspace.model.User;
import com.spring.agilspace.service.ContactService;
import com.spring.agilspace.service.HomeService;
import com.spring.agilspace.service.MenuService;
import com.spring.agilspace.service.SMTPService;
import com.spring.agilspace.service.UserService;
import com.spring.agilspace.util.Constants;

@Component
public class DataSeedingListener implements ApplicationListener<ContextRefreshedEvent>{
	
	@Autowired
	UserService userService;
	
	@Autowired
	ContactService contactService;
	
	@Autowired
	MenuService menuService;
	
	@Autowired
	SMTPService smtpService;
	
	@Autowired
	HomeService homeService;

	@Override
	public void onApplicationEvent(ContextRefreshedEvent arg0) {
		
		//initial only 1 user admin
		List<User> lsUser = userService.getAllUser();
		if(lsUser == null || lsUser.isEmpty()){
			User admin = new User();
			admin.setId(1);
			admin.setUsername(Constants.DEFAULT_AUTHEN);
			admin.setPassword(Constants.DEFAULT_AUTHEN);
			admin.setIsAdmin(Constants.ROLE_ADMIN);
			admin.setEmail(Constants.DEFAULT_EMAIL);
			admin.setPhone(Constants.DEFAULT_PHONE);
			admin.setAddress(Constants.DEFAULT_ADDRESS);
			userService.saveUser(admin);
		}
		
		//init home page
		List<Home> lsHome = homeService.getHome();
		if(lsHome == null || lsHome.isEmpty()){
			Home home = new Home();
			home.setAppName(Constants.APP_NAME);
			home.setIntroduction(Constants.APP_INTRO);
			home.setExplore(Constants.EXPLORE);
			home.setStart(Constants.START);
			home.setSingaportintro(Constants.SINGAPORE_INTRO);
			home.setBackground1(Constants.FOLDER_IMAGE.concat(File.separator).concat(Constants.BACKGROUND_FILE_1));
			home.setBackground2(Constants.FOLDER_IMAGE.concat(File.separator).concat(Constants.BACKGROUND_FILE_2));
			homeService.saveHome(home);
		}
		
		//initial Contact
		List<Contact> lsContact = contactService.getContact();
		if(lsContact == null || lsContact.isEmpty()){
			Contact contact = new Contact();
			contact.setPageTitle(Constants.CONTACT_US);
			contact.setElectronics(Constants.ELECTRONICS);
			contact.setEmailAddress(Constants.DEFAULT_EMAIL);
			contact.setPhoneNumber(Constants.DEFAULT_PHONE);
			contact.setAddress(Constants.DEFAULT_ADDRESS);
			contact.setSend(Constants.SEND);
			contact.setNameform(Constants.NAME);
			contact.setEmailform(Constants.EMAIL_ADDRESS);
			contact.setContactform(Constants.CONTACT_NUMBER);
			contact.setCompanyform(Constants.COMPANY_NAME);
			contact.setEnquiry(Constants.ENQUIRY);
			contact.setEnquiryoption(Constants.ENQUIRY_OPTION);
			contact.setMessage(Constants.MESSAGE);
			contactService.saveContact(contact);
		}
		//init menu
		List<Menu> lsMenu = menuService.getMenu();
		if(lsMenu == null || lsMenu.isEmpty()){
			Menu menu = new Menu();
			menu.setAbout(Constants.ABOUT);
			menu.setGeoportal(Constants.GEOPORTAL);
			menu.setGeoservices(Constants.GEOSERVICES);
			menu.setGeoserviceSub1(Constants.DATE_WAREHOUSE);
			menu.setGeoserviceSub2(Constants.INSIGHTS);
			menu.setGeoserviceSub3(Constants.GEOAPPS_SOFTWARE);
			menu.setGeoserviceSub4(Constants.TRAINING_CONSULTANCY);
			menu.setSatelite(Constants.SATELLITE);
			menu.setSateliteSub1(Constants.TELEOS_SERIES);
			menu.setSateliteSub2(Constants.PARTNERS);
			menu.setIntegratedService(Constants.INTERGRATED_SERVICES);
			menu.setServiceSub1(Constants.PROGRAME_MANAGEMENT);
			menu.setServiceSub2(Constants.LAUNCH_MANAGEMENT);
			menu.setServiceSub3(Constants.GCS);
			menu.setNews(Constants.NEWS);
			menu.setNewsSub1(Constants.CALENDAR);
			menu.setNewsSub2(Constants.ENEWSLETTER);
			menu.setNewsSub3(Constants.GALLERY);
			menu.setContact(Constants.CONTACT);
			menuService.saveMenu(menu);
		}
		//init SMTP email
		List<SMTPEmail> lsSMTP = smtpService.getEmail();
		if(lsSMTP == null || lsSMTP .isEmpty()){
			SMTPEmail smtpEmail = new SMTPEmail();
			smtpEmail.setEmail(Constants.SMTP_EMAIL);
			smtpEmail.setPassword(Constants.SMTP_PASSWORD);
			smtpService.saveEmail(smtpEmail);
		}
	}

}
