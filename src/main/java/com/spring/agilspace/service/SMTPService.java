package com.spring.agilspace.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.spring.agilspace.model.SMTPEmail;
import com.spring.agilspace.repository.SMTPEmailRepository;

@Service
public class SMTPService {

	@Autowired
	SMTPEmailRepository smtpEmailRepository;
	
	public void saveEmail(SMTPEmail smtpEmail){
		smtpEmailRepository.save(smtpEmail);
	}
	
	public List<SMTPEmail> getEmail(){
		return smtpEmailRepository.findAll();
	}
}
