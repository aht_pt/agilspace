package com.spring.agilspace.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.spring.agilspace.model.EmailSender;
import com.spring.agilspace.repository.EmailSenderRepository;

@Service
public class EmailSenderService {

	@Autowired
	EmailSenderRepository emailSenderRepository;
	
	public void saveEmailSender(EmailSender emailSender){
		emailSenderRepository.save(emailSender);
	}
	
	public List<EmailSender> getEmailSender(){
		return emailSenderRepository.findAll();
	}
}
