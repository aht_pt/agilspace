package com.spring.agilspace.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.spring.agilspace.model.Partner;
import com.spring.agilspace.repository.PartnerRepository;

@Service
public class PartnerService {
	
	@Autowired
	PartnerRepository partnerRepository;
	
	public void savePartner(Partner partner){
		partnerRepository.save(partner);
	}
	
	public Partner findByName(String name){
		return partnerRepository.findByName(name);
	}
	
	public List<Partner> getPartner(){
		return partnerRepository.findAll();
	}
	
	public Partner findPartner(int id){
		return partnerRepository.findOne(id);
	}
	
	public void deletePartner(Partner partner){
		partnerRepository.delete(partner);
	}
}
