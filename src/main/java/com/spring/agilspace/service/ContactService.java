package com.spring.agilspace.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.spring.agilspace.model.Contact;
import com.spring.agilspace.repository.ContactRepository;

@Service
public class ContactService {
	
	@Autowired
	ContactRepository contactRepository;
	
	public void saveContact(Contact contact){
		contactRepository.save(contact);
	}
	
	public List<Contact> getContact(){
		return contactRepository.findAll();
	}
}
