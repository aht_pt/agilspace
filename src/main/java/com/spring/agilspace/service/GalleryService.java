package com.spring.agilspace.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.spring.agilspace.model.Gallery;
import com.spring.agilspace.repository.GalleryRepository;

@Service
public class GalleryService {

	@Autowired
	GalleryRepository galleryRepository;
	
	public void saveGallery(Gallery gallery){
		galleryRepository.save(gallery);
	}
	
	public List<Gallery> getGallery(){
		return galleryRepository.findAll();
	}
	
	public Gallery findGallery(int id){
		return galleryRepository.findOne(id);
	}
	public void deleteGallery(Gallery gallery){
		galleryRepository.delete(gallery);
	}
}
