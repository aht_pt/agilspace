package com.spring.agilspace.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.spring.agilspace.model.Satellite;
import com.spring.agilspace.repository.SatelliteRepository;

@Service
public class SatelliteService {

	@Autowired
	SatelliteRepository satelliteRepository;
	
	public void saveSatellite(Satellite satellite){
		satelliteRepository.save(satellite);
	}
	public List<Satellite> getSatellite(){
		return satelliteRepository.findAll();
	}
	public List<Satellite> getByPartner(int partnerid){
		return satelliteRepository.findByPartnerid(partnerid);
	}
	
	public void deleteSatellite(Satellite satellite){
		satelliteRepository.delete(satellite);
	}
	
	public Satellite findSatellite(int id){
		return satelliteRepository.findOne(id);
	}
	
	public Satellite searchSatellite(int partnerid, String name){
		return satelliteRepository.searchSatellite(partnerid, name);
	}
}
