package com.spring.agilspace.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.spring.agilspace.model.Menu;
import com.spring.agilspace.repository.MenuRepository;

@Service
public class MenuService {

	@Autowired
	MenuRepository menuRepository;
	
	public void saveMenu(Menu menu){
		menuRepository.save(menu);
	}
	
	public List<Menu> getMenu(){
		return menuRepository.findAll();
	}
	
	public Menu findMenu(int id){
		return menuRepository.findOne(id);
	}
}
