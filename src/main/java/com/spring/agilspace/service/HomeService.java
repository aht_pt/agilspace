package com.spring.agilspace.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.spring.agilspace.model.Home;
import com.spring.agilspace.repository.HomeRepository;

@Service
public class HomeService {

	@Autowired
	HomeRepository homeRepository;
	
	public void saveHome(Home home){
		homeRepository.save(home);
	}
	
	public List<Home> getHome(){
		return homeRepository.findAll();
	}
}
