package com.spring.agilspace.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.spring.agilspace.model.About;
import com.spring.agilspace.repository.AboutRepository;

@Service
public class AboutService {

	@Autowired
	AboutRepository aboutRepository;
	
	public void saveAbout(About about){
		aboutRepository.save(about);
	}
	
	public List<About> getAbout(){
		return aboutRepository.findAll();
	}
}
