jQuery(document).ready(function(){
	$('#tablepartners').dataTable();
	$('#tblpartners').dataTable();
	$('#tblsatellites').dataTable();
	$("#div_focus").focus();
	jQuery('.add-new').hide();
	jQuery('.btn-add-new').click(function () {
        jQuery('.add-new').show();
    });

    jQuery('.btn-close-new').click(function () {
        jQuery('.add-new').hide();
    });
    jQuery('.add-new-satellite').hide();
	jQuery('.btn-add-new-satellite').click(function () {
        jQuery('.add-new-satellite').show();
    });

    jQuery('.btn-close-new-satellite').click(function () {
        jQuery('.add-new-satellite').hide();
    });
    jQuery('.add-new-gallery').hide();
	jQuery('.btn-add-new-gallery').click(function () {
        jQuery('.add-new-gallery').show();
    });

    jQuery('.btn-close-new-gallery').click(function () {
        jQuery('.add-new-gallery').hide();
    });
    
    $(document).on('change','.up', function(){
	   	var names = [];
	   	var length = $(this).get(0).files.length;
		    for (var i = 0; i < $(this).get(0).files.length; ++i) {
		        names.push($(this).get(0).files[i].name);
		    }
		    // $("input[name=file]").val(names);
			if(length>2){
			  var fileName = names.join(', ');
			  $(this).closest('.form-group').find('.form-control').attr("value",length+" files selected");
			}
			else{
				$(this).closest('.form-group').find('.form-control').attr("value",names);
			}
   });
    $(".btn[data-target='#editPartnerModal']").click(function() {
		var $row = $(this).closest("tr");
		var $partnerid = $row.find(".id").text();
		var $partnername = $row.find(".name").text();
		$("#partneridedit").val($partnerid);
		$("#partnernameedit").val($partnername);
	});
    $(".btn[data-target='#showSatellite']").click(function() {
		var $row = $(this).closest("tr");
		var $partnerid = $row.find(".id").text();
		var $partnername = $row.find(".name").text();
		$("#ModalSatelliteLabel").val($partnername);
		$.ajax({
			type : "GET",
			contentType : "application/json",
			url : "/admin/satellite/" + $partnerid,
			dataType: 'text',
			success: function (data) {
				$('.modal-body-satellite').html(data);
			},
			error: function (e) {
			}
		});
	});
    $(".fullstat").hide();
    $('input[type=radio][name=display]').change(function() {
    	if (this.value == 'image') {
            $(".onlyimage").show();
            $(".fullstat").hide();
        }else if (this.value == 'full') {
        	$(".fullstat").show();
        	$(".onlyimage").hide();
        }
    });
});
